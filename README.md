Data Infomation:

Title: A Medical History of British India
Description: This dataset contains 1 plain text readme file; 1 inventory CSV file; 117,022 ALTOXML files; 468 METS files; 120,903 image files.
Owner: National Library of Scotland
Creator: National Library of Scotland
Date created: 27/08/2019
Rights: Item-level rights information can be found in the METS files. Items in this dataset are free of known copyright and in the public domain.
Contact: digital.scholarship@nls.uk


Data Analysis:

I choose a book "74461102-Cholera in India, 1862 to 1881" to analysis.

As the data has three forms: images, xmls and txt. I read some of the Images to make sure the content and recover the districts of a clear sheet at the beginning of the book to see whether it can show something clear. - Tool to analysis: glob, elementree.

(1) Create a chart that show the times of the districts showed in the book, and see that the times is totally different whcih means many data of the distrcts may lost or did not be collected.

(2) Create a wordcloud to analysis the word frquency. It can be seen that the "Bengal province" has the most times just behind the "cholera", after that is the "district".

(3)After analysing the whole book, I can see the whole book described the diseases happened in Bengal province. And I found the table in the end of the book is the summary of the overall data, and the last part is the data about the European troops, Native troops and prisoners in jails which attracted me.  Then I use xml position to recover the sheet about this part and see some rend of these three groups infection and caculate the death rate.
